#ifdef ZPU
#include "putchar.h"
#else
#include <stdio.h>
#define xprintf printf
#endif

template <int N, typename valuetype = unsigned, int width = sizeof(valuetype)*8> 
struct bitset
{
    valuetype bits[(N + width - 1) / width];

    bitset() : bits() {}

    void set(int n)
    {
        bits[n / width] |= 1 << (n % width);
    }

    bool test(int n) const
    {
        return (bits[n / width] & (1 << (n % width)))
            ? true : false;
    }

    void print()
    {
        int n = 0;
        for (int i = 0; i < sizeof(bits)/sizeof(bits[0]); ++i) {
            valuetype w = bits[i];
            for (valuetype j = 1; j; j <<= 1) {
                if ((w & j) == 0) {
                    xprintf("%d ", n);
                }
                ++n;
            }
        }
    }
};

template <int N>
void sieve()
{
    bitset<N> bs;
    bs.set(0);
    bs.set(1);
    for (int i = 2; i*i < N; ++i) {
        if (!bs.test(i)) {
            //xprintf("%d ", i);
            int j = i, hole;
            while( (hole = i * j) < N) {
                bs.set(hole);
                ++j;
            }
        }
    }

    bs.print();
}

#define N 8192

int main()
{
    xprintf("sieve %d\n", N);
    sieve<N>();

    return 0;
}
