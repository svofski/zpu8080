// originally from http://paulbourke.net/fractals/lorenz/
// by Paul Burke

// NB This is hilariously slow, but it's a useful floating point test.

#if ZPU

#include "putchar.h"
#include "xprintf.h"

#else

#include <stdio.h>
#define xprintf printf

#endif

#ifdef MEGATRACE
#define N 10
#else
#define N 1000
#endif

#define SCREEN_ADDR 0xa000

unsigned char * const brr = (unsigned char *) SCREEN_ADDR;

volatile unsigned long long W;

void test(unsigned x, unsigned y)
{
    unsigned long long w = (unsigned long long)x * (unsigned long long)y;
    W = w;
    xprintf("%u %u\n", (unsigned)(w>>32), (unsigned)w);
}

void setpixel(int x, int y)
{
    unsigned xofs = x / 8 * 256;
    unsigned yofs = y % 256;
    unsigned bit = x % 8; 

#ifndef TESTBENCH
    brr[xofs + yofs] |= (0x80>>bit);
#endif
}

int main(int argc,char **argv)
{
    int i=0;
    float x0,y0,z0,x1,y1,z1;
    float h = 0.01;
    float a = 10.0;
    float b = 28.0;
    float c = 8.0 / 3.0;

    x0 = 0.1;
    y0 = 0;
    z0 = 0;
    puts("LOLRENZ");

    test(65535UL, 65535UL*35UL);

#ifdef MEGATRACE
    unsigned * x0p = &x0, * y0p = &y0, * z0p = &z0;
#endif

#ifdef TESTBENCH
    for (i=0;i<N;i++) {
#else
    for(i = 0;;++i) {
#endif
#ifdef MEGATRACE
        float tmp = y0 - x0;
        xprintf("x0=%d y0=%d y0-x0=%d ", 
                (int)(x0*10000), (int)(y0*10000), (int)(tmp*10000));

        tmp = h * a * tmp;
        xprintf("h*a*(y0-x0)=%d ", (int)(tmp*10000));

        x1 = x0 + tmp;
        xprintf("x0+h*a*(y0-x0)=%d ", (int)(x1*10000));
#else
        x1 = x0 + h * a * (y0 - x0);
#endif
        y1 = y0 + h * (x0 * (b - z0) - y0);
        z1 = z0 + h * (x0 * y0 - c * z0);
        x0 = x1;
        y0 = y1;
        z0 = z1;
        int xi = 118 + (int)(x0 * 6);
        int yi = 110 + (int)(y0 * 4);
#ifdef TESTBENCH
#ifdef MEGATRACE
        xprintf("x0=%08x y0=%08x z0=%08x  %03d %03d %03d\n", 
                *x0p, *y0p, *z0p,
                (int)(x0*10000), (int)(y0*10000), (int)(z0*10000));
#else
        xprintf("%03d %03d %03d\n", i, xi, yi);
#endif
#endif
        setpixel(xi, yi);
    }
    puts("doneski");
    return 0;
}
