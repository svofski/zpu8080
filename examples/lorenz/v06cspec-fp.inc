VM_START        .equ $6000
INIT_SP         .equ $90F8
INIT_ZSP        .equ $a000

#define MACH_V06C 1
#undef MACH_CPM

FlipTab         .equ $7200
#define INSN_FLIP 1
#define FLIP_TABULATED

#define SYSCALL_MEMCPY      1
#define SYSCALL_STRLEN      1
#define SYSCALL_STRNCMP     1
#define SYSCALL_STRCHR      1

; From the build script/makefile:
; provide VM_CODE_SIZE      sizeof(hirom)
; provide ZPU_CODE_START    $100 + sizeof(start) + sizeof(hirom)
; provide ZPU_CODE_SIZE     sizeof(zpu.0000)
