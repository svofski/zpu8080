#!/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
hm = np.genfromtxt('lorenz.csv', delimiter=' ')
plt.plot(hm[:,1],hm[:,2])
plt.show()
