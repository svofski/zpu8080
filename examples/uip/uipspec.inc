VM_START        .equ $6a00
;INIT_SP         .equ $90F8
INIT_ZSP        .equ $8000

; FLIP is not needed
#undef INSN_FLIP
#define MACH_CPM 1

; enable accelerated libc and ethernet driver syscalls
#define SYSCALL_ETHERNET    1
#define SYSCALL_MEMCPY      1
#define SYSCALL_STRLEN      1
#define SYSCALL_STRNCMP     1
#define SYSCALL_STRCHR      1
#define SYSCALL_CHKSUM      1

; From the build script/makefile:
; provide VM_CODE_SIZE      sizeof(hirom)
; provide ZPU_CODE_START    $100 + sizeof(start) + sizeof(hirom)
; provide ZPU_CODE_SIZE     sizeof(zpu.0000)

