#
#include "asm.h"

! int eth_rx(uint8_t *buf)

.define _eth_rx
_eth_rx:
    pop h           ! pop return address
    pop d           ! pop parameter
    push d
    push h          ! keep stack balance

    push b          ! save FP

    ! d = parameter, buffer
    xchg            ! hl -> buffer, de = junk
    in 7
    mov d, a
    mov b, a
    in 7
    mov e, a
    mov c, a        ! de = return value, don't touch
    ora b
    jz .1

.2:
    in 7
    mov m, a
    inx h
    dcx b
    mov a, b
    ora c
    jnz .2

.1: 
    pop b           ! restore FP
    ret

