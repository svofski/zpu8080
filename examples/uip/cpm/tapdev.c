/*
 * Copyright (c) 2001, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Adam Dunkels <adam@sics.se>
 *
 * $Id: tapdev.c,v 1.8 2006/06/07 08:39:58 adam Exp $
 */

#include <stdio.h>
#include <stdint.h>
#include "uip.h"
#include "stddef.h"
#include <string.h>
#include "tapdev.h"

#define TAP_INIT         1


//#define ETH_BUF_ADDR 0xa000
//
//uint8_t * const uip_buf = (uint8_t *) ETH_BUF_ADDR;

#define SYSCALL_ETH_WRCSR   5
#define SYSCALL_ETH_RDCSR   6
#define SYSCALL_ETH_RX      7
#define SYSCALL_ETH_TX      8

static void csr_write(uint8_t v)
{
}

static uint8_t csr_read(void)
{
    return 0;
}

extern int eth_rx(uint8_t *buf);
extern void eth_tx(uint8_t * buf, int txlen);


/*---------------------------------------------------------------------------*/
void
tapdev_init(void)
{
    csr_write(TAP_INIT);

    printf("tapdev_init: UIP_BUFSIZE=%d\n", UIP_BUFSIZE);
}

/*---------------------------------------------------------------------------*/
unsigned int
tapdev_read(void)
{
    int i;
    size_t cnt = eth_rx(uip_buf);
//    if (cnt) {
//        printf("read: %u %x\n", cnt, uip_buf);
//        for (i = 0; i < cnt; ++i) {
//            printf("%02x ", uip_buf[i]);
//        }
//        putchar('\n');
//    }
    return cnt;
}

void tapdev_cts()
{
}
/*---------------------------------------------------------------------------*/
void
tapdev_send(void)
{
//    printf("send: %d\n", uip_len);
    eth_tx(uip_buf, uip_len);
}
/*---------------------------------------------------------------------------*/
