#
#include "asm.h"

! void setled(uint8_t n)

.define _setled
_setled:
    pop h       ! pop return address
    pop d       ! param param param
    push d
    push h
    mov a, e
    out 0
    mvi a, 0xc9
    sta 0x38
    ret

