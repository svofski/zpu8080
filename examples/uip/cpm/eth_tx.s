#
#include "asm.h"

! void eth_tx(uint8_t *buf, int len)

.define _eth_tx
_eth_tx:
    pop d           ! pop return addr
    pop h           ! pop param 1: buf
    shld dst
    pop h           ! pop param 2: len
    shld cnt
    push h
    push h
    push d

    push b          ! save FP

cnt = . + 1
    lxi b, 0
    mov a, b
    out 6           ! msb of length
    mov a, c
    out 6           ! lsb of length

dst = . + 1
    lxi h, 0

.1:
    mov a, m
    inx h
    out 6
    dcx b
    mov a, b
    ora c
    jnz .1

    pop b           ! restore FP
    ret
