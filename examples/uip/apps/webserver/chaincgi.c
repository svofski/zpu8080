#include "httpd.h"
#include "uip.h"
#include "chaincgi.h"

static uint8_t led_var = 3 << 1;

void chain_led_get(struct httpd_state *s)
{
    *((uint8_t*)uip_appdata + s->len++) = '0' + (led_var & 1);
}


void chain_led_toggle(struct httpd_state *s)
{
    extern void setled(int);
    setled(led_var ^= 1);
    *((uint8_t*)uip_appdata + s->len++) = '0' + (led_var & 1);
}
