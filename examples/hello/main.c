#include "xprintf.h"

int main()
{
    int x = 0x12345678;
    int y = 0x87654321;

    xprintf("Hello from ZPU! Please be patient.\n");
    xprintf("x=%08d y=%08d\n", x, y);

    for (unsigned i = 0; i < 72; ++i) {
        putchar('A' + ((i ^ (i << (i % 32))) % 26));
    }

    xprintf("\n");

    for (int i = 1; i < 10; ++i) {
        for (int j = 1; j < 10; ++j) {
            xprintf("%2d ", i * j);
        }
        xprintf("\n");
    }

    xprintf("Bye!\n");

    return 0;
}
