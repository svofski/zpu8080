/* $Source$
 * $State$
 * $Revision$
 */

/* Adapted from code by Chris Losinger. (This is the non-obfuscated
 * version...
 * 
 * http://www.codeproject.com/cpp/mandelbrot_obfuscation.asp
 */

#include "putchar.h"
 
enum
{
	ROWS = 40,
	COLUMNS = 60,
	
	MAX_ITERATIONS = 255
};

void nl(void)
{
	putchar('\n');
	putchar('\r');
}

void out(int n)
{
	const char* chars = "****++++++----  ";
        // the right way putchar(chars[n/16]);
        putchar(chars[n&15]); // the cool way
}

// 1 = 1000
// (1 * 1)/1000

#define SCALE (65536)
#define SQRT  (256)

void intmand()
{
    int view_r = -2.3 * SCALE, view_i = -1.0 * SCALE;
    int zoom = 0.05 * SCALE;
    int x, y, n;

    for (y=0; y < ROWS; y++)
    {
        int c_i = view_i + y*zoom;
        
        for (x=0; x < COLUMNS; x++)
        {
            int c_r = view_r + x*zoom;
            int z_r = c_r;
            int z_i = c_i;
            
            for (n=0; n < MAX_ITERATIONS; n++)
            {
                int z_r2 = z_r / SQRT * z_r / SQRT;
                int z_i2 = z_i / SQRT * z_i / SQRT;

                /* Have we escaped? */
                if (z_r2 + z_i2 > 4 * SCALE)
                    break;

                /* z = z^2 + c */
                z_i = 2 * z_r / SQRT * z_i / SQRT + c_i;
                z_r = z_r2 - z_i2 + c_r;
            }

            out(n);
        }
        nl();
    }
}

int main(int argc, const char* argv[])
{
    intmand();
    
    return 0;
}
