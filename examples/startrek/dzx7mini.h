#pragma once
#ifdef ZPU
#include "putchar.h"
#endif

#ifdef ACK
#include <stdint.h>
#endif

typedef struct {
    const unsigned char * src;
} zx7mini_t;

// callback for decode_stream
// 0 = continue, -1 = stop decoding
typedef int (*stream_cb)(void *, int);

void dzx7mini_init(zx7mini_t * z, const unsigned char * the);
void dzx7mini_decode(zx7mini_t *z, unsigned char * dst);
void dzx7mini_decode_stream(zx7mini_t *z, uint8_t * buffer, 
        stream_cb cb, void * user);

