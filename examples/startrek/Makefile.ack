MAKEROOT ?= $(realpath ../..)
BINDIR ?= $(MAKEROOT)/bin
ZX7MINI := $(MAKEROOT)/tools/zx7/zx7mini

TOOLCHAIN ::= /opt/ack/bin
override CC ::= $(TOOLCHAIN)/ack
CCFLAGS ::= -O6 -mcpm -I$(MAKEROOT)/examples/common -DACK=1 -ansi

TARGET := startrek.com

OFILES := startrek.o dzx7mini.o startrek_doz.o

EXTRALIBS ?= -lc

include $(MAKEROOT)/common.mk

vpath %.c $(BUILDDIR)

$(ZX7MINI):	$(ZX7MINI).c
	make CC=gcc -C $(dir $(ZX7MINI)) zx7mini

$(BUILDDIR)/startrek.doz:	startrek.doc makedirs
	$(ZX7MINI) $< $@

$(BUILDDIR)/startrek_doz.c:	$(BUILDDIR)/startrek.doz
	# objcopy creates names with full paths, so changedir to $(BUILDDIR)
	cd $(BUILDDIR) && \
	xxd -i $(notdir $<) > $(notdir $@)

$(BUILDDIR)/startrek.com: $(addprefix $(BUILDDIR)/, $(OFILES))
	$(CC) -o $@ $^ -mcpm \
	    $(addprefix $(TOOLCHAIN)/../share/ack/cpm/,boot.o c-ansi.o)\
	    -lc.a -lsys.a -lem.a -lsys.a -lend.a

$(BUILDDIR)/%.s:	%.c
	$(CC) -c.s $(CCFLAGS) $< -o $@

