#ifdef UNIX
#include <stdio.h>
#include <stdint.h>
#endif

#ifdef ACK
#include <stdio.h>
#include <stdint.h>
#endif

#include "dzx7mini.h"

void dzx7mini_init(zx7mini_t * z, const unsigned char * the)
{
    z->src = the;
}

static uint16_t getbit(zx7mini_t *z, uint16_t a)
{
    uint16_t b;
    uint16_t aa = (255 & a) << 1;
    if (aa & 255) return aa;
    b = *z->src++;
    b <<= 1;
    if (aa & 0x100) b |= 1;
    return b;
}

void dzx7mini_decode(zx7mini_t *z, unsigned char * dst)
{
    uint16_t a = 0x80;
    uint16_t c;
    uint8_t *span;

    while(1) {
        *dst++ = *z->src++;
        do {
            a = getbit(z, a);
            if ((a & 0x100) == 0) break;

            c = 1;
            while(1) {
                a = getbit(z, a);
                c <<= 1;
                if (a & 0x100) c |= 1;
                if (c & 0x100) return;
                a = getbit(z, a);
                if (a & 0x100) break;
            }

            *span = dst - *z->src - 1;
            for (; c > 0; --c) {
                *dst++ = *span++;
            }
            ++z->src;
        } while (1);
    }
}


// buffer size is 256
void dzx7mini_decode_stream(zx7mini_t *z, uint8_t * buffer, 
        stream_cb cb, void * user)
{
    uint16_t a = 0x80;
    uint16_t c;
    int dofs = 0;
    int back;

    while(1) {
        if (cb(user, buffer[dofs++] = *z->src++)) return;
        dofs &= 255;

        do {
            a = getbit(z, a);
            if ((a & 0x100) == 0) break;

            c = 1;
            while(1) {
                a = getbit(z, a);
                c <<= 1;
                if (a & 0x100) c |= 1;
                if (c & 0x100) return;
                a = getbit(z, a);
                if (a & 0x100) break;
            }

            back = dofs - *z->src - 1;
            if (back < 0) back += 256;
            for (; c > 0; --c) {
                if (cb(user, buffer[dofs++] = buffer[back++])) return;
                dofs &= 255;
                back &= 255;
            }
            ++z->src;
        } while (1);
    }
}

#ifdef UNIX

void putnik(void *_, int c)
{
    putchar(c);
}

int main()
{
    uint8_t the[4096];

    FILE * f = fopen("startrek.doz", "r");
    fread(the, sizeof(the), 1, f);
    fclose(f);
    
    zx7mini_t z;
    dzx7mini_init(&z, &the[0]);

    uint8_t buf[256];
    dzx7mini_decode_stream(&z, buf, putnik, NULL);

    return 0;
}
#endif
