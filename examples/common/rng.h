#pragma once

#ifdef __cplusplus
extern "C" {
#endif

void srand(int n);
int rand(void);

#ifdef __cplusplus
}
#endif
