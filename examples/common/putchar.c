#include "putchar.h"
#include "xprintf.h"

extern int _syscall(int *foo, int ID, ...);

int putchar(int c)
{
    if (c == '\n') putchar('\r');
    return _syscall((int *)1, 2, c);
}

int getchar()
{
    int c = _syscall((int *)1, 3);
    if (c == '\r') c = '\n';
    return c;
}

int entropy()
{
    return _syscall((int *)1, 4);
}


int chdir(const char * s)
{
    return 0;
}

int puts(const char * str)
{
    _syscall((int *)1, 1, str);
    putchar('\n');
    return 0;
}

int fputs(const char * str, int fd)
{
    _syscall((int *)1, 1, str);
    return 0;
}

int strncmp(const char *s1, const char *s2, size_t n)
{
    return _syscall((int *)1, 11, s1, s2, n);
}

int setled(int n)
{
    return _syscall((int *)1, 12, n);
}


//int strncmp(const char *s1, const char *s2, size_t n)
//{
//  unsigned char u1, u2;
//
//  while (n-- > 0)
//    {
//      u1 = (unsigned char) *s1++;
//      u2 = (unsigned char) *s2++;
//      if (u1 != u2)
//        return u1 - u2;
//      if (u1 == '\0')
//        return 0;
//    }
//  return 0;
//}


size_t strlen(const char *s)
{
    return _syscall((int*)1, 10, s);
}

char * strchr(const char *s, int c)
{
    return (char *)_syscall((int*)1, 13, s, c);
}

//char *strcat(char * s1, const char *s2)
//{
//    char *s = s1;
//    while (*s1) ++s1;
//    while (*s1++ = *s2++);
//    return s;
//}
//
//char * strcpy(char * dst, const char * src)
//{
//    while (*src) {
//        *dst++ = *src++;
//    }
//    *dst++ = 0;
//}

int atoi(const char *s)
{
    while ((*s) != 0 && (*s == ' ' || *s == '\t' || *s == '\n' || *s == '\r')) 
        ++s;
    if (!*s) return 0;
    int sign = 1;
    if (*s == '-') { sign = -1; ++s; }
    if (*s == '+') { ++s; }
    int ans = 0;
    while (*s) {
        if (*s >= '0' && *s <= '9') {
            int k = *s - '0';
            ans = ans * 10 + k;
        }
        else {
            break;
        }
        ++s;
    }
    if (sign < 0) {
        ans = -ans;
    }
    return ans;
}

int tolower(int c)
{
    if (c >= 'A' && c <= 'Z') return c + ('a'-'A');
    return c;
}

int isdigit(int c)
{
    return c >= '0' && c <= '9';
}

int abs(int n)
{
    return n >= 0 ? n : -n;
}

time_t time(time_t *tloc)
{
    return entropy();
}

void * memcpy(void * out, void * in, size_t length)
{
    _syscall((int *)1, 9, out, in, length);
    return out;
}


void * memset(void * out, int c, size_t len)
{
    uint8_t * out8 = (uint8_t *)out;
    for (unsigned i = 0; i < len; ++i) {
        *out8++ = c;
    }
    return out;
}

// save on _impure_ptr
void exit(int status)
{
    __asm__ volatile (("breakpoint"));
}

