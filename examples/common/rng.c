#define LFSR_TYPE unsigned short
#define LFSR_POLY 0xb400

int lcg_seed;

LFSR_TYPE lfsr(LFSR_TYPE init)
{
    static LFSR_TYPE lfsr;
    if (init) {
        lfsr = init;
    }

    LFSR_TYPE new_lfsr = lfsr >> 1;
    if (lfsr & 1) {
        new_lfsr ^= LFSR_POLY;
    }
    lfsr = new_lfsr;
    return lfsr;
}

void srand(int n)
{
    lcg_seed = n;
    lfsr(n);
}

int rand()
{
    lcg_seed = lcg_seed * 134775813 + lfsr(0);
    return lcg_seed;
}


