#pragma once

#include "xprintf.h"
#include "rng.h"

#include "stddef.h"
#include "inttypes.h"

#ifdef __cplusplus
extern "C" {
#endif

#define EOF (-1)
#if !defined(NULL)
#define NULL ((void *)0)
#endif
#define stdout 1

#define printf xprintf
#define sprintf xsprintf

#ifndef __time_t_defined
typedef unsigned int time_t;
#define __time_t_defined
#endif

// syscalls
int putchar(int c);
int getchar();
int entropy();

int chdir(const char * s);
int puts(const char * str);
int fputs(const char * str, int fd);
int strncmp(const char *s1, const char *s2, size_t n);
size_t strlen(const char *s);
char *strcat(char * s1, const char *s2);
int atoi(const char *s);
int tolower(int c);
int isdigit(int c);
int abs(int n);
time_t time(time_t *tloc);
void * memcpy(void * out, void * in, size_t length);
void * memset(void * out, int c, size_t len);
char * strcpy(char * dst, const char * src);
void exit(int status);

#ifdef __cplusplus
}
#endif
