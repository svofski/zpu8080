datasz  .equ 4096/2
dezx7sz .equ 60
        .org $100
        di
        xra a
        out $10
        lxi sp, data0
        lxi h, HIMEM
        lxi b, $ffff-datasz 
loop:
        pop d
        mov m, e \ inx h
        mov m, d \ inx h
        inr c
        jnz loop
        inr b
        jnz loop
        lxi sp, 0
        lxi h, HIMEM + dezx7sz
        lxi d, $100
        push d
        jmp HIMEM
data0:
        .end
