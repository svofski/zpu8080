#define NINTS 1024
#define TYPE int
#if !(defined(TESTBENCH))
#define TESTBENCH 0
#endif

#ifdef ZPU
#include "putchar.h"

#define SCREEN_ADDR 0xa000

TYPE * const arr = (TYPE *) SCREEN_ADDR;
unsigned char * const barr = (unsigned char *) arr;

#else
#include <stdlib.h>
#include <stdio.h>
#include <cstring>
#define xprintf printf
TYPE arr[NINTS];
unsigned char * const barr = (unsigned char *)arr;
#endif

template <typename T>
void printarr(T * arr, int low, int high)
{
    for (int i = low; i <= high; ++i) {
        xprintf("%d ", arr[i]);
    }
    xprintf("\n");
}

template <typename valuetype>
void swap(valuetype & a, valuetype & b)
{
    valuetype tmp = a; 
    a = b; b = tmp;
}

template <typename valuetype>
struct quicksort {

    int partition(valuetype * arr, int low, int high) const
    {
        valuetype pivot = arr[high];

        int i = low;
        for (int j = low; j < high; ++j) {
            if (arr[j] < pivot) {
                swap(arr[i], arr[j]);
                ++i;
            }
        }
        swap(arr[i], arr[high]);
        return i;
    }

    // save cpu cycles and stack by making the checks before recursing deeper
    void sort(valuetype * arr, int low, int high) const
    {
        int mid = partition(arr, low, high);
        if (mid - 1 > low) {
            if (mid - 1 - low < 16)
                bobble(arr, low, mid - 1);
            else
                sort(arr, low, mid - 1);
        }
        if (high > mid + 1) {
            if (high - mid - 1 < 16)
                bobble(arr, mid + 1, high);
            else
                sort(arr, mid + 1, high);
        }
    }

    void bobble(valuetype * arr, int low, int high) const
    {
        int new_high;
        do {
            new_high = low;
            for (int j = low + 1; j <= high; ++j) {
                if (arr[j-1] > arr[j]) {
                    swap(arr[j-1], arr[j]);
                    new_high = j;
                }
            }
            high = new_high;
        } while (high - low > 1);
    }

    void sort(valuetype * arr, int n) const
    {
        sort(arr, 0, n - 1);
    }
};

void shuffle_data()
{
    const int n = sizeof(TYPE) * NINTS;
    const int m = n / 2;
    for (int i = 0; i < m; ++i) {
        int j = ((unsigned)rand()) % m;
        swap(barr[i], barr[m + j]);
        swap(barr[j], barr[n - i - 1]);
    }

// knuth-like shuffle, but it uses too much much modulo,
// which is crazy slow for zpu8080 
// also i suspect an error error in modulo somewhere
//    for (int i = sizeof(TYPE) * NINTS - 1; i > 0; --i) {
//        int j = ((unsigned)rand()) % i;
//        swap(barr[j], barr[i]);
//    }
}

// create predetermined data and shuffle
void prepare_shuffled_data()
{
    srand(1);

    for (int i = 1; i < NINTS; ++i) {
        int q = i >> 2;
        arr[i] = (q << 24) | (q << 16) | (q << 8) | q;
    }
    shuffle_data();
}

void prepare_random_data()
{
    srand(1);
    for (int i = 1; i < NINTS; ++i) {
        arr[i] = rand();
    }
}

int main()
{
    xprintf("ZPU8080\nQSORT DEMO");
    for (int i = 0; i < 26; ++i) putchar('\n');
    xprintf("SVO    2020\n\nH/T! IVAGOR\014\n\n\n");

#if SIEVE
    xprintf("%d SIEVE\n\n", NINTS);
    sieve<NINTS*32>();
    invert_data();
#else
    prepare_random_data();
#endif

    while (1) {
        xprintf("%d BYTES\n\n", NINTS * sizeof(TYPE));
        quicksort<unsigned char>().sort((unsigned char *)arr, NINTS * sizeof(TYPE));

#if TESTBENCH
        return 0;
#endif

        xprintf("\n\nDONE!\n\nSHUFFLING\014\n\n\n");
        shuffle_data(); 
        for (int i = 0; i < 16; ++i) xprintf("          \n");
        xprintf("\014\n\n\n");
    }

#ifndef ZPU
    printarr(arr, 0, NINTS-1);
#else
    //printarr(arr, 0, NINTS-1);
    //for(;;);
#endif

    return 0;
}
