FROM multiarch/alpine:x86-v3.12 AS test
COPY patches/*.patch /tmp/
RUN apk update && apk add npm wget build-base flex gperf linux-headers xxd
ENV PATH=${PATH}:/tmp/zpugcc-6714b053f6c701ab3fe75f87f7daa16f345780c3/toolchain/install/bin
WORKDIR /tmp
RUN (wget -q -O- https://github.com/zylin/zpugcc/archive/6714b053f6c701ab3fe75f87f7daa16f345780c3.tar.gz | tar zxv ) && \
    cd zpugcc-6714b053f6c701ab3fe75f87f7daa16f345780c3 && \
    patch -p 1 </tmp/11.patch && \
    patch -p 1 </tmp/udivmod.patch && \
    patch -p 1 </tmp/libgcc-Os.patch && \
    patch -p 1 </tmp/neq.patch && \
    patch -p 1 </tmp/combine.patch && \
    patch -p 1 </tmp/costs.patch && \
    patch -p 1 </tmp/make-j.patch && \
    patch -p 1 </tmp/fpadd_parts.patch && \
    patch -p 1 </tmp/umulsidi3.patch && \
    cd toolchain && ./fixperm.sh && \
    sh ./build.sh && \
    mv install /zpugcc && \
    cd .. && rm -rf /tmp/*

WORKDIR /tmp
# don't rm this in /tmp this because npm is stupid enough to "install" by creating symlinks
RUN (wget -q -O- https://github.com/svofski/bin2wav/archive/67940cfff1170adb25cc545ec9f92ce15fa498c9.tar.gz | tar zxv) && \
    cd bin2wav-67940cfff1170adb25cc545ec9f92ce15fa498c9 && \
    npm install -g
WORKDIR /tmp
RUN (wget -q -O- wget https://github.com/svofski/prettyasm/archive/tasm-like2.tar.gz | tar zxv) && \
    cd prettyasm-tasm-like2 && \
    npm install -g
WORKDIR /zpu8080
