export TOOLCHAIN ?= $(HOME)/zpugcc/toolchain/install/bin
export CC := $(TOOLCHAIN)/zpu-elf-gcc
export CXX := $(TOOLCHAIN)/zpu-elf-g++
export OBJCOPY ?= $(TOOLCHAIN)/zpu-elf-objcopy
export OBJDUMP ?= $(TOOLCHAIN)/zpu-elf-objdump
export SIZE ?= $(TOOLCHAIN)/zpu-elf-size
#ASM := wine tasm
export ASM ?= pasm
export BOARD ?= -abel

INC := -I$(MAKEROOT)/examples/common
BUILDDIR := build

export OPT ?= -Os -fno-if-conversion

EXTRALIBS ?= 

CCFLAGS ?= -mumod -mudiv -std=c99 -ffunction-sections -DZPU=1 $(INC)
CXXFLAGS ?= -mumod -mudiv -ffunction-sections -DZPU=1 $(INC)
ASFLAGS ?= -mumod -mudiv -ffunction-sections -DZPU=1 $(INC)
LDFLAGS ?= $(BOARD) -nostdlib -Wl,--relax -Wl,--gc-sections -Wl,--trace -DZPU=1

ifneq ($(MEGATRACE),)
    CCFLAGS += -DMEGATRACE=1
    CXXFLAGS += -DMEGATRACE=1
endif

# default spec for .com programs
CPMSPEC ?= $(MAKEROOT)/core/cpmspec.inc

# default spec for .rom programs
ROMSPEC ?= $(MAKEROOT)/core/v06cspec.inc

VPATH := $(MAKEROOT)/examples/common

$(BUILDDIR)/%.o:	%.cpp makedirs
	$(CXX) -c $(CXXFLAGS) $(OPT) $(EXTRAS) $< -o $@

$(BUILDDIR)/%.o:	%.c makedirs
	$(CC) -c $(CCFLAGS) $(OPT) $(EXTRAS) $< -o $@

.PRECIOUS: $(BUILDDIR)/%.o
$(BUILDDIR)/%.o:	$(MAKEROOT)/startup/%.S makedirs
	$(CC) -c $(ASFLAGS) $(EXTRAS) $< -o $@

$(BINDIR)/%.rom:	$(BUILDDIR)/%.rom
	cp $< $@$(SUFFIX)

$(BINDIR)/%.wav:	$(BUILDDIR)/%.wav
	cp $< $@$(SUFFIX)

$(BINDIR)/%.com:	$(BUILDDIR)/%.com
	cp $< $@$(SUFFIX)

$(BUILDDIR)/%.inc:	$(BUILDDIR)/%.0000
	cat $^ | hexdump -v -e '16/1 "%02x " "\n"' | \
	    awk 'BEGIN {FS=" "} {printf("    .db "); for (i = 1; i <= NF; i++) printf("$$%s%c", $$i, i==NF?"\n":","); }' >$@


.PRECIOUS:	$(BUILDDIR)/%.0000
$(BUILDDIR)/%.0000:	$(BUILDDIR)/%.elf
	$(OBJCOPY) -O binary $< $@
	$(OBJDUMP) -D -S $< >$(basename $@).txt

.PRECIOUS:	$(BUILDDIR)/%.elf
$(BUILDDIR)/%.elf:	$(addprefix $(BUILDDIR)/, $(OFILES))
	$(CXX) -o $@ $(LDFLAGS) -Wl,-Map=$@.map $^ $(EXTRALIBS)
	$(SIZE) $@

.PRECIOUS: $(BUILDDIR)/%.rom
$(BUILDDIR)/%.rom:	$(BUILDDIR)/%.0000 makedirs
	ln -f -s $(MAKEROOT)/core/TASM85.TAB $(BUILDDIR)
	cp $(MAKEROOT)/core/hirom.asm $(BUILDDIR)
	cp $(ROMSPEC) $(BUILDDIR)/machine.inc
	cp $(MAKEROOT)/core/fastim.inc $(BUILDDIR)
	cp $(MAKEROOT)/core/body.inc $(BUILDDIR)
	cp $(MAKEROOT)/core/start.asm $(BUILDDIR)
	cp $< $(BUILDDIR)/guestcode.inc
	# 1. build hirom to calculate its size
	# 2. build start
	# 3. rebuild hirom knowing start size
	(   set -e -x; cd $(BUILDDIR); \
	    ZPUSIZE=$$(cat $(notdir $<) | wc -c) ; \
	    $(ASM) $(EXTRAS) -DZPU_CODE_START=0 -DZPU_CODE_SIZE=$$ZPUSIZE -b -t85 hirom.asm hirom.bin; \
	    VMSIZE=$$(cat hirom.bin | wc -c); \
	    echo "VM_CODE_SIZE=$$VMSIZE"; \
	    $(ASM) $(EXTRAS) -DVM_CODE_SIZE=$$VMSIZE -b -t85 start.asm start.bin;\
	    STARTSIZE=$$(cat start.bin | wc -c); \
	    ZSTART=$$((256+VMSIZE+STARTSIZE)); \
	    echo "ZSTART=$$ZSTART"; \
	    $(ASM) $(EXTRAS) -DZPU_CODE_START=$$ZSTART -DZPU_CODE_SIZE=$$ZPUSIZE -b -t85 hirom.asm hirom.bin; \
	    cat start.bin hirom.bin $(notdir $<) >$(notdir $@); \
	    )


.PRECIOUS: $(BUILDDIR)/%.com
$(BUILDDIR)/%.com:	$(BUILDDIR)/%.0000 makedirs
	ln -f -s $(MAKEROOT)/core/TASM85.TAB $(BUILDDIR)
	cp $(MAKEROOT)/core/hirom.asm $(BUILDDIR)
	cp $(CPMSPEC) $(BUILDDIR)/machine.inc
	cp $(MAKEROOT)/core/fastim.inc $(BUILDDIR)
	cp $(MAKEROOT)/core/body.inc $(BUILDDIR)
	cp $(MAKEROOT)/core/start.asm $(BUILDDIR)
	cp $< $(BUILDDIR)/guestcode.inc
	# 1. build hirom to calculate its size
	# 2. build start
	# 3. rebuild hirom knowing start size
	(   set -e -x; cd $(BUILDDIR); \
	    ZPUSIZE=$$(cat $(notdir $<) | wc -c) ; \
	    $(ASM) $(EXTRAS) -DZPU_CODE_START=0 -DZPU_CODE_SIZE=$$ZPUSIZE -b -t85 hirom.asm hirom.bin; \
	    VMSIZE=$$(cat hirom.bin | wc -c); \
	    echo "VM_CODE_SIZE=$$VMSIZE"; \
	    $(ASM) $(EXTRAS) -DVM_CODE_SIZE=$$VMSIZE -b -t85 start.asm start.bin;\
	    STARTSIZE=$$(cat start.bin | wc -c); \
	    ZSTART=$$((256+VMSIZE+STARTSIZE)); \
	    echo "ZSTART=$$ZSTART"; \
	    $(ASM) $(EXTRAS) -DZPU_CODE_START=$$ZSTART -DZPU_CODE_SIZE=$$ZPUSIZE -b -t85 hirom.asm hirom.bin; \
	    cat start.bin hirom.bin $(notdir $<) >$(notdir $@); \
	    )

.DEFAULT:	all

all:	makedirs $(addprefix $(BINDIR)/,$(TARGET))

# testbench is the first target compiled with extra flags
testbench:	EXTRAS=-DTESTBENCH=1
testbench:	SUFFIX=_tb
#testbench:	$(word 1,$(addprefix $(BINDIR)/,$(TARGET)))
#	make -C $(MAKEROOT)/testbench build
#	$(MAKEROOT)/testbench/i8080_test $<_tb
testbench:	$(word 1,$(addprefix $(BUILDDIR)/,$(TARGET)))
	make -C $(MAKEROOT)/testbench build
	mv $< $<_tb
	$(MAKEROOT)/testbench/i8080_test $<_tb

makedirs:
	mkdir -p $(BUILDDIR) $(BINDIR)

clean:
	rm -rf *.o *.0000 *.elf "$(BUILDDIR)"


