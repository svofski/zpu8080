        ; ZPU virtual machine for 8080 CPU (Vector-06c)
        ; 2020 Viacheslav Slavinsky
        ;      Ivan Gorodetsky
        ;
        ; ZPU is a soft CPU specification by Øyvind Harboe at Zylin AG
        ;

        .include "machine.inc"


        .org VM_START
sbrk:
        jmp begin


begin:
        lxi h, fulldispatch_unaligned
        lxi d, fulldispatch
shuffle_dispatch:
        mov a, m \ stax d \ inx h \ inr d           ; lsb to first half
        mov a, m \ stax d \ inx h \ dcr d \ inr e   ; msb to second half
        jnz shuffle_dispatch

#ifdef MACH_CPM
        ; reuse original dispatch table to store microdos junk
        call save_dos
#endif

#ifdef MACH_V06C
        lxi sp, INIT_SP

        #ifndef TESTBENCH
        call setup_display
        #endif
#endif

        lxi h, ZPU_CODE_START
        lxi d, 0
        lxi b, ZPU_CODE_SIZE / 4 + 257
reloc0:
        mov a, m \ stax d \ inx h \ inx d
        mov a, m \ stax d \ inx h \ inx d
        mov a, m \ stax d \ inx h \ inx d
        mov a, m \ stax d \ inx h \ inx d
        dcr c \ jnz reloc0
        dcr b \ jnz reloc0

#ifdef FLIP_TABULATED
        lxi d,FlipTab
MakeFlipTab:
        mov h,e
        dad h\ rar
        dad h\ rar
        dad h\ rar
        dad h\ rar
        dad h\ rar
        dad h\ rar
        dad h\ rar
        dad h\ rar
        stax d
        inr e
        jnz MakeFlipTab
#endif

        ; main array at a000
        ; b000 halfscreen
        ; c000 right side of screen
zpuvm_enter:
        lxi h, 0
        dad sp
        shld savedSP
        lxi sp,0 ; ZPC
        lxi b,INIT_ZSP
		jmp decode_exit
decode_exit_sp:
        pop b
decode_exit_sp_:
ZPC     .equ $+1
        lxi sp,0
decode_exit:        ; normal insns return here
zpuvm_loop1:
fulltable_decode:
        pop h \ dcx sp          ;L=opcode
        mvi h, fulldispatch>>8
        mov e, m                ; lsb of dispatch
        inr h                   ; hl->second half of dispatch table
        mov d, m                ; msb of dispatch
        xchg
        pchl                    ; jump to insn microcode, opcode in a


        .include "body.inc"


#ifdef MACH_V06C
setup_display:
        ; mode 256
        mvi a, $0
        out 2

        ; clear screen
        lxi h, $a000
        lxi d, $c000
clrscr_l1:
        xra a
clrscr_l2:
        mov m, a
        stax d
        inx d
        inr l
        jnz clrscr_l2
        inr h
        mov a, h
        cpi $c0
        jnz clrscr_l1

        call set_palette

        lxi h, $10f8
        jmp gotoxy

putchar:
        mov a, e
        cpi $d
        jz putchar_cr
        cpi $a
        jz putchar_nl
        cpi $c
        jnz _putchar_a
        lxi h, $10f8
        jmp gotoxy

putchar_cr:
        mvi a, $10
        sta _puts_de+1
        ret
putchar_nl:
        lda _puts_de
        sui 8
        sta _puts_de
        ret

getchar:
        mvi l, 0
        mov a, l
        ret

    ; установить позицию для вывода следующего символа
    ; H = столбец, L = строка ($F8 = верхняя)
gotoxy:
        shld _puts_de
        ret

_puts_de:   .dw 0
        ; Нарисовать один символ
_putchar_a:
        ora a
        rz
        xchg
        lhld _puts_de
        inr h
        shld _puts_de
        xchg

        lxi h,0
        dad sp
        shld __savedsp+1

        ; Найти адрес спрайта символа
        ; bc = (c-32)*8
        ;hl=a*8+_font_table-(32*8)
        mov l,a
        mvi h,0
        dad h\ dad h\ dad h
        lxi b, _font_table-(32*8)
        dad b
        sphl        ; sp -> char

        lxi h, $a000
        dad d        ; hl -> destination

        ; Выдавить биты на экран
        pop b\ mov m, c\ dcr l\ mov m, b\ dcr l
        pop b\ mov m, c\ dcr l\ mov m, b\ dcr l
        pop b\ mov m, c\ dcr l\ mov m, b\ dcr l
        pop b\ mov m, c\ dcr l\ mov m, b

__savedsp:
    lxi sp,0
        mvi a, 1
        ret

set_palette:
    ; Программирование палитры
        mvi     a,0C9h
        sta     38h
        ei
        hlt
        lxi     h, colors+15
colorset:
        mvi     a, 88h
        out     0
        mvi     c, 15
colorset1:      mov     a, c
        out     2
        mov     a, m
        out     0Ch
        xthl
        xthl
        xthl
        xthl
        out     0Ch
        dcx     h
        dcr c
        jp      colorset1
        mov a,c ;255
        out     3
        ret

        ; surface a000-bfff, bit 2
        ; 0000 0
        ; 0001 0
        ; 0010 1
        ; 0011 1
        ; 0100 0
        ; 0101 0
        ; 0110 1
colors:
        .db 00, 00, 0, 0,40, 40, 0, 0
        .db 4, 4, 0, 0,0, 0, 0, 0


_font_table:
    ; Font: 8X8!FONT.pf
    .db    $00, $00, $00, $00, $00, $00, $00, $00
    .db    $30, $78, $78, $30, $30, $00, $30, $00
    .db    $6C, $6C, $6C, $00, $00, $00, $00, $00
    .db    $6C, $6C, $FE, $6C, $FE, $6C, $6C, $00
    .db    $30, $7C, $C0, $78, $0C, $F8, $30, $00
    .db    $00, $C6, $CC, $18, $30, $66, $C6, $00
    .db    $38, $6C, $38, $76, $DC, $CC, $76, $00
    .db    $60, $60, $C0, $00, $00, $00, $00, $00
    .db    $18, $30, $60, $60, $60, $30, $18, $00
    .db    $60, $30, $18, $18, $18, $30, $60, $00
    .db    $00, $66, $3C, $FF, $3C, $66, $00, $00
    .db    $00, $30, $30, $FC, $30, $30, $00, $00
    .db    $00, $00, $00, $00, $00, $30, $30, $60
    .db    $00, $00, $00, $FC, $00, $00, $00, $00
    .db    $00, $00, $00, $00, $00, $30, $30, $00
    .db    $06, $0C, $18, $30, $60, $C0, $80, $00
    .db    $7C, $C6, $CE, $DE, $F6, $E6, $7C, $00
    .db    $30, $70, $30, $30, $30, $30, $30, $00
    .db    $78, $CC, $0C, $38, $60, $C0, $FC, $00
    .db    $78, $CC, $0C, $38, $0C, $CC, $78, $00
    .db    $1C, $3C, $6C, $CC, $FE, $0C, $0C, $00
    .db    $FC, $C0, $F8, $0C, $0C, $CC, $78, $00
    .db    $38, $60, $C0, $F8, $CC, $CC, $78, $00
    .db    $FC, $0C, $0C, $18, $30, $30, $30, $00
    .db    $78, $CC, $CC, $78, $CC, $CC, $78, $00
    .db    $78, $CC, $CC, $7C, $0C, $18, $70, $00
    .db    $00, $30, $30, $00, $00, $30, $30, $00
    .db    $00, $30, $30, $00, $00, $30, $30, $60
    .db    $18, $30, $60, $C0, $60, $30, $18, $00
    .db    $00, $00, $FC, $00, $00, $FC, $00, $00
    .db    $60, $30, $18, $0C, $18, $30, $60, $00
    .db    $78, $CC, $0C, $18, $30, $00, $30, $00
    .db    $7C, $C6, $DE, $DE, $DE, $C0, $78, $00
    .db    $18, $3C, $66, $66, $7E, $66, $66, $00
    .db    $7C, $66, $66, $7C, $66, $66, $7C, $00
    .db    $3C, $66, $C0, $C0, $C0, $66, $3C, $00
    .db    $78, $6C, $66, $66, $66, $6C, $78, $00
    .db    $7E, $60, $60, $78, $60, $60, $7E, $00
    .db    $7E, $60, $60, $78, $60, $60, $60, $00
    .db    $3C, $66, $C0, $C0, $CE, $66, $3E, $00
    .db    $66, $66, $66, $7E, $66, $66, $66, $00
    .db    $18, $18, $18, $18, $18, $18, $18, $00
    .db    $06, $06, $06, $06, $66, $66, $3C, $00
    .db    $66, $66, $6C, $78, $6C, $66, $66, $00
    .db    $60, $60, $60, $60, $60, $60, $7E, $00
    .db    $C6, $EE, $FE, $FE, $D6, $C6, $C6, $00
    .db    $C6, $E6, $F6, $DE, $CE, $C6, $C6, $00
    .db    $3C, $66, $66, $66, $66, $66, $3C, $00
    .db    $7C, $66, $66, $7C, $60, $60, $60, $00
    .db    $3C, $66, $66, $66, $6E, $3C, $0E, $00
    .db    $7C, $66, $66, $7C, $6C, $66, $66, $00
    .db    $3C, $66, $70, $38, $0E, $66, $3C, $00
    .db    $7E, $18, $18, $18, $18, $18, $18, $00
    .db    $66, $66, $66, $66, $66, $66, $3E, $00
    .db    $66, $66, $66, $66, $66, $3C, $18, $00
    .db    $C6, $C6, $C6, $D6, $FE, $EE, $C6, $00
    .db    $66, $66, $3C, $18, $3C, $66, $66, $00
    .db    $66, $66, $66, $3C, $18, $18, $18, $00
    .db    $FE, $06, $0C, $18, $30, $60, $FE, $00
    .db    $78, $60, $60, $60, $60, $60, $78, $00
    .db    $C0, $60, $30, $18, $0C, $06, $02, $00
    .db    $78, $18, $18, $18, $18, $18, $78, $00
    .db    $07, $0E, $39, $63, $83, $31, $1C, $07
    .db    $C0, $E0, $38, $8C, $82, $18, $70, $C0
#endif  ; MACH_V06C

#ifdef MACH_CPM
call5:
        ; install dummy rst handler
        mvi a, $c9
        sta $38
        lda saved_params+5
        sta 5
        lhld saved_params+6
        shld 6
        jmp 5

save_dos:
        lxi h, 0
        lxi d, saved_params
saveloop:
        mov a, m \ stax d
        inx d
        inr l
        jnz saveloop
        ret

restore_dos:
        lxi h, 0
        lxi d, saved_params
rest_loop:
        ldax d \ mov m, a
        inx d
        inr l
        jnz rest_loop
        ret

putchar:
        mvi c, 2
        call call5
        di
        lxi h, 5
        shld $-6
        mvi a, $c9
        sta $-8
        ret

getchar:
        lhld saved_params+$38
        shld $38
        lhld saved_params+$3a
        shld $3a
        lda saved_params+5
        sta 5
        lhld saved_params+6
        shld 6

getchar_poll:
        mvi c, 6
        mvi e, $ff
        call 5
        ora a
        jnz getchar_gotchar
entropy_counter equ $ + 1
        lxi h, 7            ;; guarranteed to be random
        inx h
        shld entropy_counter
        jmp getchar_poll
getchar_gotchar:
        push psw
        mvi c, 2
        mov e, a
        call 5
        pop psw
        mov l, a
        di
        ret


        ;mov a, e \ out 1 \ ret
saved_params .equ fulldispatch_unaligned
;saved_params:
#endif
        .org $ + 255 & ~255
fulldispatch:
        .end

