    ; startup code
    ; requires VM_START (defined in machine.inc)
    ; requires VM_CODE_SIZE (must be provided as -DVM_CODE_SIZE=...)
    .include "machine.inc"

    .org $100
    di
#ifdef MACH_V06C
    xra a \ out $10
#endif
    ; move vm code to its running location
    lxi h, vm_code_start
    lxi d, VM_START
    lxi b, VM_CODE_SIZE / 4 + 257
reloc0:
    mov a, m \ stax d \ inx h \ inx d
    mov a, m \ stax d \ inx h \ inx d
    mov a, m \ stax d \ inx h \ inx d
    mov a, m \ stax d \ inx h \ inx d
    dcr c \ jnz reloc0
    dcr b \ jnz reloc0
    jmp VM_START
vm_code_start:

    .end
