        ; faster im group decoder by ivagor
        ; gives ~1% speed increase on quicksort, but is significantly larger
insn_im:
im_first:
#ifndef HALFTABLE_DECODER
	mov b,a
	adc a
#else
        mov a, b
        add a
#endif
	jp im_positive

im_negative:
;	lhld ZPC_NEXT
	ana m
	jp im_neg1
	inx h
	ana m
	jp im_neg2
	inx h
	ana m
	jp im_neg3
	inx h
	ana m
	jp im_neg4
;im_neg5
	inx h

	;shld ZPC
	inx h
	shld ZPC_NEXT
	xchg
	dcx d
	dcx d

        lhld ZSP
	ldax d\ dcx d\ ral\ mov b,a
	ldax d\ dcx d\ rar\ mov c,a
	mov a,b\ rar\ dcx h\ mov m,a
	mvi a,00111111b\ ana c\ mov c,a
	ldax d\ dcx d\ rrc\ rrc\ mov b,a\ ani 11000000b\ ora c\ dcx h\ mov m,a
	mvi a,00011111b\ ana b\ mov b,a
	ldax d\ dcx d\ rrc\ rrc\ rrc\ mov c,a\ ani 11100000b\ ora b\ dcx h\ mov m,a
	mvi a,00001111b\ ana c\ mov c,a
	ldax d\ add a\ add a\ add a\ add a\ ora c\ dcx h\ mov m,a
	jmp predecode_2

im_neg1:
        ;shld ZPC
        inx h
        shld ZPC_NEXT

        mvi c, $ff
        xchg
        lhld ZSP
        dcx h \ mov m, b \ dcx h \ mov m, c \ dcx h \ mov m, c \ dcx h \ mov m, c
        shld ZSP
        xchg
        dcx h
        jmp predecode_2_
		
im_neg2:
        ;shld ZPC
        inx h
        shld ZPC_NEXT
        xchg
        dcx d
        dcx d

        lhld ZSP
        ldax d\ ral\ mov c,a
        mov a,b\ rar\ mov b,a
        mov a,c\ rar\ dcx h\ mov m,a
        dcx h\ mov m,b
        dcx h\ mvi m,0FFh
        dcx h\ mvi m,0FFh
        shld ZSP
        xchg
        inx h
        jmp predecode_2_
		

im_neg3:
        ;shld ZPC
        inx h
        shld ZPC_NEXT
        xchg
        dcx d
        dcx d

        lhld ZSP
        ldax d\ dcx d\ ral\ mov b,a
        ldax d\ dcx d\ rar\ mov c,a
        mov a,b\ rar\ dcx h\ mov m,a
        mvi a,00111111b\ ana c\ mov c,a
        ldax d\ rrc\ rrc\ mov b,a\ ani 11000000b\ ora c\ dcx h\ mov m,a
        mvi a,11100000b\ ora b\ dcx h\ mov m,a
        dcx h\ mvi m,0FFh
        jmp predecode_2	

im_neg4:
        ;shld ZPC
        inx h
        shld ZPC_NEXT
        xchg
        dcx d
        dcx d

        lhld ZSP
        ldax d\ dcx d\ ral\ mov b,a
        ldax d\ dcx d\ rar\ mov c,a
        mov a,b\ rar\ dcx h\ mov m,a
        mvi a,00111111b\ ana c\ mov c,a
        ldax d\ dcx d\ rrc\ rrc\ mov b,a\ ani 11000000b\ ora c\ dcx h\ mov m,a
        mvi a,00011111b\ ana b\ mov b,a
        ldax d\ rrc\ rrc\ rrc\ mov c,a\ ani 11100000b\ ora b\ dcx h\ mov m,a
        mvi a,11110000b\ ora c\ dcx h\ mov m,a
        jmp predecode_2	

im_positive:
;       lhld ZPC_NEXT
	cma
	ana m
	jp im_pos1
	inx h
	ana m
	jp im_pos2
	inx h
	ana m
	jp im_pos3
	inx h
	ana m
	jp im_pos4
;im_pos5
	inx h

	;shld ZPC
	inx h
	shld ZPC_NEXT
	xchg
	dcx d
	dcx d

        lhld ZSP
	ldax d\ dcx d\ ral\ mov b,a
	ldax d\ dcx d\ rar\ mov c,a
	mov a,b\ rar\ dcx h\ mov m,a
	mvi a,00111111b\ ana c\ mov c,a
	ldax d\ dcx d\ rrc\ rrc\ mov b,a\ ani 11000000b\ ora c\ dcx h\ mov m,a
	mvi a,00011111b\ ana b\ mov b,a
	ldax d\ dcx d\ rrc\ rrc\ rrc\ mov c,a\ ani 11100000b\ ora b\ dcx h\ mov m,a
	mvi a,00001111b\ ana c\ mov c,a
	ldax d\ add a\ add a\ add a\ add a\ ora c\ dcx h\ mov m,a
	jmp predecode_2

	
im_pos2:
	;shld ZPC
	inx h
	shld ZPC_NEXT
	xchg
	dcx d
	dcx d

        lhld ZSP
	ldax d\ ral\ mov c,a
	mov a,b\ rar\ mov b,a
	mov a,c\ rar\ dcx h\ mov m,a
	mvi a,00111111b\ ana b\ dcx h\ mov m,a
	xra a
	dcx h\ mov m,a
	dcx h\ mov m,a
	shld ZSP
	xchg
	inx h		
	jmp predecode_2_

im_pos3:
	;shld ZPC
	inx h
	shld ZPC_NEXT
	xchg
	dcx d
	dcx d

        lhld ZSP
	ldax d\ dcx d\ ral\ mov b,a
	ldax d\ dcx d\ rar\ mov c,a
	mov a,b\ rar\ dcx h\ mov m,a
	mvi a,00111111b\ ana c\ mov c,a
	ldax d\ rrc\ rrc\ mov b,a\ ani 11000000b\ ora c\ dcx h\ mov m,a
	mvi a,00011111b\ ana b\ dcx h\ mov m,a
	dcx h\ mvi m,0
	jmp predecode_2	

im_pos4:
	;shld ZPC
	inx h
	shld ZPC_NEXT
	xchg
	dcx d
	dcx d

        lhld ZSP
	ldax d\ dcx d\ ral\ mov b,a
	ldax d\ dcx d\ rar\ mov c,a
	mov a,b\ rar\ dcx h\ mov m,a
	mvi a,00111111b\ ana c\ mov c,a
	ldax d\ dcx d\ rrc\ rrc\ mov b,a\ ani 11000000b\ ora c\ dcx h\ mov m,a
	mvi a,00011111b\ ana b\ mov b,a
	ldax d\ rrc\ rrc\ rrc\ mov c,a\ ani 11100000b\ ora b\ dcx h\ mov m,a
	mvi a,00001111b\ ana c\ dcx h\ mov m,a
	jmp predecode_2	

im_pos1:
	;shld ZPC
	inx h
	shld ZPC_NEXT
	mvi a,00111111b
	ana b

        lhld ZSP
        dcx h \ mov m, a \ dcx h \ xra a\ mov m, a \ dcx h \ mov m, a \ dcx h \ mov m, a
	;jmp predecode_2	

predecode_2:
	shld ZSP
	lhld ZPC_NEXT \ dcx h
predecode_2_:
	mov a,m	

