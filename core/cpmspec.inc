VM_START        .equ $6000
;INIT_SP         .equ $90F8
INIT_ZSP        .equ $6000
PUTCHAR_OUT     .equ $7fff

; FLIP is sometimes not needed
;FlipTab         .equ $7200
#define INSN_FLIP 1
;#define FLIP_TABULATED 1
#define MACH_CPM 1

#define SYSCALL_MEMCPY      1
#define SYSCALL_STRLEN      1
#define SYSCALL_STRNCMP     1
#define SYSCALL_STRCHR      1

; From the build script/makefile:
; provide VM_CODE_SIZE      sizeof(hirom)
; provide ZPU_CODE_START    $100 + sizeof(start) + sizeof(hirom)
; provide ZPU_CODE_SIZE     sizeof(zpu.0000)

