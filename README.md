ZPU8080
=======

This is a ZPU virtual machine written in Intel 8080 assembly. It executes normal ZPU code on a 8080 computer with 64K RAM. The primary target is Vector-06c. It is pretty slow, but allows writing C++ code for a 8080 computer, which is cool. Example projects include [visual quicksort demo](/examples/sortdemo), a port of [Star Trek](/examples/startrek) game and an IoT-style [webserver](/examples/uip) using uIP TCP/IP stack ([demo video](https://www.youtube.com/watch?v=Ylp_YzykKCY))

Building examples using docker.io
=====================================
This project requires many difficult to keep dependencies, for example zpugcc with custom patches. To make it easy to build, there's a docker.io recipe provided that can be used for building and development. To build the image, run:
```
docker build -t svo/zpugcc .
```
To build all examples:
```
docker run -it -v$(pwd):/zpu8080 --rm svo/zpugcc make TOOLCHAIN=/zpugcc/bin
```
Consult files `docker-build.sh` and `docker-make.sh`.

Building examples without docker
=====================
To build the examples you'll need a Linux machine and installed zpugcc. It is possible to use regular zpu-gcc, but this project benefits greatly when the compiler can generate UDIV/UMOD. Make sure the path zpu-elf-gcc is set (see common.mk), and type make. See below for specific details.

UDIV/UMOD and zpu-gcc patch
===========================
Standard ZPU implements signed div/mod but strangely not unsigned. When the compiler encounters an unsigned division and modulo operations, it generates calls to __udivmodsi3, which results in slow program execution. Since performance in this project is already quite silly, losing every little bit is prohibitive. I added two new instructions to this version of ZPU and added them to gcc. 

It is possible to use unpatched zpugcc, but unsigned division/modulo will suffer.

TASM
====
The assembly portions of the project are compiled using offline [prettyasm](https://github.com/svofski/prettyasm/). The syntax used is a subset of Telemark TASM 3.1 and the pasm can be substituted with tasm.

Links
=====
 * zpugcc https://github.com/zylin/zpugcc/
 * UDIV/UMOD patch https://github.com/zylin/zpugcc/issues/16
 * TASM https://github.com/feilipu/NASCOM_BASIC_4.7/tree/master/TASM31
