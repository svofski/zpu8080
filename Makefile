all:	examples

export MAKEROOT := $(shell pwd)
export BINDIR := $(MAKEROOT)/bin


.PHONY:	examples

examples:
	make -C examples

clean:
	rm -f tools/zx7/zx7mini
	make -C examples clean
	make -C testbench clean

distclean:
	rm -rf $(BINDIR)
